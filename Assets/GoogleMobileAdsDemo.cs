﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GoogleMobileAds;
using GoogleMobileAds.Api;
using System;

public class GoogleMobileAdsDemo : MonoBehaviour
{

    public Text adUnitIDText;
    public Text debug;

    private RewardBasedVideoAd rewardBaseVideoAd;
    private string adUnitId;

    private BannerView bannerView;
    // Use this for initialization
    void Start()
    {
        rewardBaseVideoAd = RewardBasedVideoAd.Instance;

        
        // init
#if UNITY_ANDROID
        string appId = "ca-app-pub-7295035519235773~3634929506";
        //string appId = "ca-app-pub-3940256099942544~3347511713";
#elif UNITY_IPHONE
            string appId = "ca-app-pub-3940256099942544~1458002511";
#else
            string appId = "unexpected_platform";
#endif

        // Initialize the Google Mobile Ads SDK.
        MobileAds.Initialize(appId);


        // Set Callback
        // Called when an ad request has successfully loaded.
        rewardBaseVideoAd.OnAdLoaded += HandleRewardBasedVideoLoaded;
        // Called when an ad request failed to load.
        rewardBaseVideoAd.OnAdFailedToLoad += HandleRewardBasedVideoFailedToLoad;
        // Called when an ad is shown.
        rewardBaseVideoAd.OnAdOpening += HandleRewardBasedVideoOpened;
        // Called when the ad starts to play.
        rewardBaseVideoAd.OnAdStarted += HandleRewardBasedVideoStarted;
        // Called when the user should be rewarded for watching a video.
        rewardBaseVideoAd.OnAdRewarded += HandleRewardBasedVideoRewarded;
        // Called when the ad is closed.
        rewardBaseVideoAd.OnAdClosed += HandleRewardBasedVideoClosed;
        // Called when the ad click caused the user to leave the application.
        rewardBaseVideoAd.OnAdLeavingApplication += HandleRewardBasedVideoLeftApplication;


        RequestBannerTop();
        RequestBannerBottom();
    }

    // Update is called once per frame
    void Update()
    {
        if(!rewardBaseVideoAd.IsLoaded())
        {
            debug.text = "load failed";
        }
        else
        {
            debug.text = "load successed";
        }

        adUnitIDText.text = adUnitId;
    }

    // Google's Sample AdUnitId
    public void LoadRewardAdGoogle()
    {
#if UNITY_ANDROID
        adUnitId = "ca-app-pub-3940256099942544/5224354917";  // google sample
#elif UNITY_IPHONE
         adUnitId = "ca-app-pub-3940256099942544/1712485313";
#else
          adUnitId = "unexpected_platform";
#endif
        debug.text = "wait request";
        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().AddTestDevice("6B73A0B11A1FD32F5C5E97D09826BA67").Build();

        // Load the rewarded video ad with the request.
        rewardBaseVideoAd.LoadAd(request, adUnitId);
    }

    // Myself Test AdUnitId
    public void LoadRewardAdTest()
    {
#if UNITY_ANDROID
        adUnitId = "ca-app-pub-7295035519235773/8988571808";
#elif UNITY_IPHONE
         adUnitId = "ca-app-pub-3940256099942544/1712485313";
#else
          adUnitId = "unexpected_platform";
#endif

        debug.text = "wait request";
        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().AddTestDevice("6B73A0B11A1FD32F5C5E97D09826BA67").Build();
        
        // Load the rewarded video ad with the request.
        rewardBaseVideoAd.LoadAd(request, adUnitId);
    }

    public void ShowRewardAd()
    {
        if(rewardBaseVideoAd.IsLoaded())
        {
            rewardBaseVideoAd.Show();
        }
        else
        {
            MonoBehaviour.print("Debug Load Ad Failed");
        }
    }

    public void HandleRewardBasedVideoLoaded(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleRewardBasedVideoLoaded event received");
    }

    public void HandleRewardBasedVideoFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        MonoBehaviour.print(
            "HandleRewardBasedVideoFailedToLoad event received with message: "
                             + args.Message);
    }

    public void HandleRewardBasedVideoOpened(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleRewardBasedVideoOpened event received");
    }

    public void HandleRewardBasedVideoStarted(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleRewardBasedVideoStarted event received");
    }

    public void HandleRewardBasedVideoClosed(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleRewardBasedVideoClosed event received");
    }

    public void HandleRewardBasedVideoRewarded(object sender, Reward args)
    {
        string type = args.Type;
        double amount = args.Amount;

        debug.text = "Rewards is " + args.Amount;
        MonoBehaviour.print(
            "HandleRewardBasedVideoRewarded event received for "
                        + amount.ToString() + " " + type);
    }

    public void HandleRewardBasedVideoLeftApplication(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleRewardBasedVideoLeftApplication event received");
    }

    private void RequestBannerTop()
    {
#if UNITY_ANDROID
        string adUnitId = "ca-app-pub-3940256099942544/6300978111";
#elif UNITY_IPHONE
            string adUnitId = "ca-app-pub-3940256099942544/2934735716";
#else
            string adUnitId = "unexpected_platform";
#endif

        // Create a 320x50 banner at the top of the screen.
        bannerView = new BannerView(adUnitId, AdSize.Banner, AdPosition.Top);

        AdRequest request = new AdRequest.Builder().Build();
        // Load the banner with the request.
        bannerView.LoadAd(request);
    }

    private void RequestBannerBottom()
    {
#if UNITY_ANDROID
        string adUnitId = "ca-app-pub-7295035519235773/7222180099";
#elif UNITY_IPHONE
            string adUnitId = "ca-app-pub-3940256099942544/2934735716";
#else
            string adUnitId = "unexpected_platform";
#endif

        // Create a 320x50 banner at the top of the screen.
        bannerView = new BannerView(adUnitId, AdSize.Banner, AdPosition.Bottom);

        AdRequest request = new AdRequest.Builder().AddTestDevice("6B73A0B11A1FD32F5C5E97D09826BA67").Build();
        // Load the banner with the request.
        bannerView.LoadAd(request);
    }
}
