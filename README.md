# GoogleAdMob_Nend_Practice
[Google AdMob Unity](https://developers.google.com/admob/unity/start)
1. Download  Ads Unity plugin
2. Import  Ads Unity plugin
3. 執行Assets > Play Services Resolver > Android Resolver > Resolve
4. 申請廣告單元
5. Initialize MobileAds
6. Load and show Ad

***

[Google AdMob Unity Mediation for Nend](https://developers.google.com/admob/unity/mediation/nend)
1. 根據步驟申請
2. Google AdMob建立Mediation Group
3. 將廣告單元與Nend獲得的apiId與slotId做綁定
4. Import the nend SDK 

***

### 備註
1. Google APIs for Android - https://developers.google.com/android/reference/com/google/android/gms/ads/AdRequest#ERROR_CODE_INTERNAL_ERROR
2. AdMob Error Code : 0，有可能是廣告單元正在審核，等一陣子就可以了。

